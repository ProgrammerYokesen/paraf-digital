<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use QrCode;

class ParafController extends Controller
{
    public function paraf($id){

      //$QR = QrCode::size(200)->generate('https://paraf.yokesen.com/c/'.$id);

      // $QR = QrCode::format('png')->merge('https://yokesen.com/images/logo-color-2.png', 0.3, true)
      //           ->size(200)->errorCorrection('H')
      //           ->generate('https://paraf.yokesen.com/c/'.$id);

      $QR = '';

      $paraf = DB::table('paraf')
              ->join('category','category.id','paraf.kategori_id')
              ->join('jenis','jenis.id','paraf.jenis_id')
              ->join('tipe','tipe.id','paraf.tipe_id')
              ->where('paraf.qr_code',$id)
              ->first();
      return view('paraf.generate',compact('QR','paraf'));
    }

    public function show($id){
      $paraf = DB::table('paraf')
              ->join('cms_users','cms_users.id','paraf.user_id')
              ->join('category','category.id','paraf.kategori_id')
              ->join('jenis','jenis.id','paraf.jenis_id')
              ->join('tipe','tipe.id','paraf.tipe_id')
              ->where('paraf.qr_code',$id)
              ->select(
                'paraf.document_id',
                'paraf.deskripsi',
                'paraf.created_at as time_signed',
                'category.category_name',
                'jenis.jenis_name',
                'cms_users.name',
                'cms_users.email',
                )
              ->first();
      return view('paraf.show',compact('paraf'));
    }
}
