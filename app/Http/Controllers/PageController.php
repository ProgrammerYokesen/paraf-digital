<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;

class PageController extends Controller
{
    public function add($id=null){
        $data = null;
        if($id){
            $getRequestId = DB::table('tujuan')->where('tujuan.id', $id)->join('request_paraf', 'request_paraf.id', 'tujuan.request_id')->first();
            $getData = DB::table('request_paraf')->where('request_paraf.id', $getRequestId->request_id)->join('tujuan', 'tujuan.request_id', 'request_paraf.id')->get();
            // dd($getData);
            $kepada = '';
            foreach($getData as $key=>$value){
                if($kepada== '' ){
                    $kepada = $value->receiver_id;
                }else{
                    $kepada = $kepada.','.$value->receiver_id;
                }
            }
            $data[] =[
                'id'=>$getData[0]->request_id,
                'title'=>$getData[0]->title,
                'document_id'=>$getData[0]->document_id,
                'surat'=>$getData[0]->surat,
                'link'=>$getData[0]->link,
                'kategori_id'=>$getData[0]->kategori_id,
                'jenis_id'=>$getData[0]->jenis_id,
                'tipe_id'=>$getData[0]->tipe_id,
                'deskripsi'=>$getData[0]->deskripsi,
                'due_date'=>$getData[0]->due_date,
                'receiverId'=>$kepada,
            ];
        }
        // dd($data[0]);
        $getTipe = DB::table('tipe')->where('status', 'active')->get();
        $getJenis = DB::table('jenis')->where('status', 'active')->get();
        $getCategory = DB::table('category')->where('status', 'active')->get();
        $getAllUsers = DB::table('cms_users')->where('id_cms_privileges', 2)->get();
        return view('pages.add',compact('getTipe','getJenis', 'getCategory', 'getAllUsers', 'data'));
    }
}
