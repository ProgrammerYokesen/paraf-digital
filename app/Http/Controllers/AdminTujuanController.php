<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminTujuanController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = true;
			$this->button_table_action = false;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = false;
			$this->button_delete = false;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "tujuan";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Judul","name"=>"id"];
			$this->col[] = ["label"=>"No Dokumen","name"=>"request_id","join"=>"request_paraf,document_id"];
			$this->col[] = ["label"=>"Status","name"=>"status"];
			$this->col[] = ["label"=>"Alasan Penolakan","name"=>"alasan_penolakan"];
			$this->col[] = ["label"=>"Kepada","name"=>"receiver_id","join"=>"cms_users,name"];
			$this->col[] = ["label"=>"Action","name"=>"id"];
			$this->col[] = ["label"=>"Lihat Paraf","name"=>"id"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'User Id','name'=>'user_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'user,id'];
			$this->form[] = ['label'=>'Status','name'=>'status','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Alasan Penolakan','name'=>'alasan_penolakan','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Request Id','name'=>'request_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'request,id'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'User Id','name'=>'user_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'user,id'];
			//$this->form[] = ['label'=>'Status','name'=>'status','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Alasan Penolakan','name'=>'alasan_penolakan','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Request Id','name'=>'request_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'request,id'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();
					$this->index_button[] = ["label"=>"Add Data","icon"=>"fa fa-plus","url"=>'add-data'];



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
					$query->where('sender_id', CRUDBooster::myID());
				}

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
				if($column_index == 0){
					$row = CRUDBooster::first($this->table,$column_value);
					$getData = DB::table('request_paraf')->join('tujuan', 'request_paraf.id', 'tujuan.request_id')->where('request_paraf.id', $row->request_id)->select('tujuan.status', 'tujuan.id AS tujuanId', 'tujuan.receiver_id', 'tujuan.sender_id','request_paraf.title')->get();
					$count = count($getData) -1;
					foreach($getData as $key=>$value){
						if($key == $count && $row->receiver_id == $value->receiver_id){
							$column_value = $value->title;
						}else{
							$column_value = '';
						}
					}
				}
				if($column_index == 5){
					$row = CRUDBooster::first($this->table,$column_value);
					$getData = DB::table('request_paraf')->join('tujuan', 'request_paraf.id', 'tujuan.request_id')->where('request_paraf.id', $row->request_id)->where('batch', $row->batch)->select('tujuan.status', 'tujuan.id AS tujuanId', 'tujuan.receiver_id', 'tujuan.sender_id')->get();
					$status = 0;
					$receiver_id = '';
					foreach($getData as $data){
						if($data->status == 2){
							$status = 2;
							$receiver_id = $data->receiver_id;
						}elseif($status == 0 && $data->status == 1 && $data->receiver_id != $data->sender_id){
							$status = 1;
							$receiver_id = $data->receiver_id;
						}elseif($status == 1 && $data->status == 1 && $data->receiver_id != $data->sender_id ){
							$status = 4;
							$receiver_id = $data->receiver_id;
						}elseif($status == 0 && $data->status ==0 && $data->receiver_id != $data->sender_id){
							$status = 0;
						}elseif($data->status == 4){
							$status = 4;
						}
					}
					// salah satu reject
					if($row->receiver_id ==  $row->sender_id || $row->status == 1){
						$column_value = "";
					}elseif($status == 2 && $row->receiver_id == $receiver_id){
						$column_value = "<a href='add-data/$column_value' class='btn btn-success accept'>Resubmit</a>";
					// all accept
					}elseif($row->status == 4 || $status == 4){
						$column_value = '';
					}elseif($status == 0 ){
						$column_value = "<a href='delete-data/$column_value' class='btn btn-danger accept'>Delete</a><a href='reminder/$row->receiver_id/$row->request_id' class='btn btn-warning accept'>Reminder</a>";
						// satu accept
					}elseif($status == 1 && $receiver_id != $row->receiver_id){
						$column_value = "<a href='reminder/$row->receiver_id/$row->request_id' class='btn btn-warning accept'>Reminder</a>";
						// all waiting
					}elseif($status == 4){
						$column_value = "<a href='generate/paraf/$column_value' class='btn btn-primary accept'>Lihat Paraf</a>";
						// resubmission
					}else{
						$column_value = '';
					}

				}

				if($column_index == 6){
					$row = CRUDBooster::first($this->table,$column_value);
					$getData = DB::table('request_paraf')->join('tujuan', 'request_paraf.id', 'tujuan.request_id')->where('request_paraf.id', $row->request_id)->select('tujuan.status', 'tujuan.id AS tujuanId', 'tujuan.receiver_id', 'tujuan.sender_id')->get();

					$lihatParaf = true;
					foreach($getData as $value){
						if($value->status != 1){
							$lihatParaf = false;
						}
					}
					if($lihatParaf == true){
						$column_value = "<a href='generate/paraf/$column_value' class='btn btn-primary accept'>Lihat Paraf</a>";
					}else{
						$column_value = '-';
					}
					if($row->sender_id == $row->receiver_id){
						$column_value = "<a href='generate/paraf/$column_value' class='btn btn-primary accept'>Lihat Paraf</a>";
					}
				}

				
				if($column_index == 2){
					if($column_value == 1){
						$column_value = 'Accepted';
					}elseif($column_value == 2){
						$column_value = 'Rejected';
					}elseif($column_value == 0){
						$column_value = 'Waiting';
					}else{
						$column_value = 'expired';
					}
				}
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 


	}