<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use CRUDBooster;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class ProcessController extends Controller
{
    public function addData(Request $request, $id=null)
    {
        // dd($request, $id);
        // $members = $request->except(['documentId', '_token', 'description', 'kategoriId', 'jenisId', 'tipeId', 'memberId']);
        $path = null;
        if ($request->file('image')) {
            date_default_timezone_set('Asia/Jakarta');
            $month = \Carbon\Carbon::parse($new->created_at)->format('m');
            $year = \Carbon\Carbon::parse($new->created_at)->format('Y');
            $file = $request->file('image');
            $uniqueFileName = str_replace(' ', '', time() . '-' . $file->getClientOriginalName());
            $path = $file->storeAs('uploads/' . CRUDBooster::myID() . '/' . date('Y-m') . '/', $uniqueFileName);
            $path = 'uploads/' . CRUDBooster::myID() . '/' . date('Y-m') . '/' . $uniqueFileName;
        }

        $receivers = $request->receiverId;
        $receiverList = '';
        foreach ($receivers as $key => $value) {
            if ($key == 0) {
                $receiverList = $value;
            } else {
                $receiverList = $receiverList . ',' . $value;
            }
        }

        // dd($receiverList);

        $qrCode = sha1(CRUDBooster::myID() . $request->documentId . $request->kategoriId . $request->jenisId . $request->tipeId
        . $receiverList) . time();
        if(!empty($id)){
            // dd($id);
            if(empty($path)){
                $updateRequest = DB::table('request_paraf')->where('id',$id)->update([
                    'document_id' => $request->documentId,
                    'deskripsi' => $request->description,
                    'kategori_id' => $request->kategoriId,
                    'jenis_id' => $request->jenisId,
                    'tipe_id' => $request->tipeId,
                    // 'surat' => $path,
                    'link'=>$request->link,
                    'qr_code' => $qrCode,
                    'title' => $request->title,
                    'due_date'=>$request->deadline,
                    'updated_at'=>date('Y-m-d H:i:s'),
                ]);
            }else{
                $updateRequest = DB::table('request_paraf')->where('id',$id)->update([
                    'document_id' => $request->documentId,
                    'deskripsi' => $request->description,
                    'kategori_id' => $request->kategoriId,
                    'jenis_id' => $request->jenisId,
                    'tipe_id' => $request->tipeId,
                    'surat' => $path,
                    'link'=>$request->link,
                    'qr_code' => $qrCode,
                    'title' => $request->title,
                    'due_date'=>$request->deadline,
                    'updated_at'=>date('Y-m-d H:i:s'),
                    ]);
            }
            $getLastBatch = DB::table('tujuan')->where('request_id', $id)->orderBy('id', 'DESC')->first();
            $nextBatch = $getLastBatch->batch + 1;
            $getAllBatch = DB::table('tujuan')->where('request_id', $id)->get();
            
            // update old request
            foreach($getAllBatch as $batch){
                $update = DB::table('tujuan')->where('id', $batch->id)->update([
                    'status'=>4
                ]);
            }

            
            foreach ($request->receiverId as $key => $value) {
                // dd($value, $id, $nextBatch);
                $insert = DB::table('tujuan')->insert([
                'receiver_id' => $value,
                'request_id' => $id,
                'sender_id'=>CRUDBooster::myID(),
                'batch'=>$nextBatch,
                'created_at'=>date('Y-m-d H:i:s')
                ]);
            }

            // input data yang request
            $insertRequestor= DB::table('tujuan')->insert([
                'receiver_id'=>CRUDBooster::myID(),
                'sender_id'=>CRUDBooster::myID(),
                'request_id'=>$id,
                'status'=>1,
                'batch'=>$nextBatch,
                'created_at'=>date('Y-m-d H:i:s')
            ]);
        }else{
            $insertRequest = DB::table('request_paraf')->insertGetId([
                'document_id' => $request->documentId,
                'deskripsi' => $request->description,
                'kategori_id' => $request->kategoriId,
                'jenis_id' => $request->jenisId,
                'tipe_id' => $request->tipeId,
                'user_id' => CRUDBooster::myID(),
                'surat' => $path,
                'link'=>$request->link,
                'qr_code' => $qrCode,
                'title' => $request->title,
                'due_date'=>$request->deadline,
                'created_at'=>date('Y-m-d H:i:s'),
                ]);
               
                foreach ($request->receiverId as $key => $value) {
                    $insert = DB::table('tujuan')->insert([
                    'receiver_id' => $value,
                    'request_id' => $insertRequest,
                    'sender_id'=>CRUDBooster::myID(),
                    'batch'=>0,
                    'created_at'=>date('Y-m-d H:i:s')
                    ]);
                }
                 // input data yang request
                 $insertRequestor= DB::table('tujuan')->insert([
                    'receiver_id'=>CRUDBooster::myID(),
                    'sender_id'=>CRUDBooster::myID(),
                    'request_id'=>$insertRequest,
                    'status'=>1,
                    'batch'=>0,
                    'created_at'=>date('Y-m-d H:i:s')
                ]);
        }
        
        CRUDBooster::redirect('admin/tujuan', 'permintaan telah dikirim', 'success');
    }

    public function preview($id)
    {
    return view('pages.preview', compact('id'));
    }

    public function detail($id)
    {
        $getData = DB::table('request_paraf')
        ->where('id', $id)
        ->first();
        $getUser = DB::table('cms_users')
        ->where('id', CRUDBooster::myId())
        ->first();
        $getTipe = DB::table('tipe')
        ->where('id', $getData->tipe_id)
        ->first();
        $getJenis = DB::table('jenis')
        ->where('id', $getData->jenis_id)
        ->first();
        $getCategory = DB::table('category')
        ->where('id', $getData->kategori_id)
        ->first();
        // $getTo = DB::table('cms_users')
        // ->where('id', $getData->tujuan_id)
        // ->first();
        $kepada = DB::table('tujuan')
        ->where('tujuan.request_id', $getData->id)
        ->join('cms_users', 'tujuan.receiver_id', 'cms_users.id')
        ->select('cms_users.name AS userName', 'tujuan.status AS requestStatus', 'tujuan.id AS tujuanId', 'tujuan.sender_id', 'tujuan.receiver_id', 'tujuan.alasan_penolakan', 'tujuan.qr_code_receiver', 'tujuan.request_id', 'tujuan.batch')
        ->orderBy('batch', 'DESC')
        ->get();
        // dd($kepada);
        $waiting = array();
        $detailRequest = array();
        $index = 0;
        foreach($kepada as $key=>$value){
            if($key == 0 || $value->batch == $kepada[$key - 1]->batch){
                $batch = (int)$value->batch;
                // dd(gettype($batch), gettype($index));
                $detailRequest[$batch][$index]=[
                    'userName'=>$value->userName,
                    'requestStatus'=>$value->requestStatus,
                    'tujuanId'=>$value->tujuanId,
                    'sender_id'=>$value->sender_id,
                    'receiver_id'=>$value->receiver_id,
                    'alasan_penolakan'=>$value->alasan_penolakan,
                    'qr_code_receiver'=>$value->qr_code_receiver,
                    'request_id'=>$value->request_id,
                    'batch'=>$value->batch,
                ];
                $index = $index + 1;
                // echo $index.'<br/>';
            }elseif($value->batch != $kepada[$key - 1]->batch){
                // dd('masuk');
                $index = 0;
                $batch = (int)$value->batch;
                $detailRequest[$batch][$index]=[
                    'userName'=>$value->userName,
                    'requestStatus'=>$value->requestStatus,
                    'tujuanId'=>$value->tujuanId,
                    'sender_id'=>$value->sender_id,
                    'receiver_id'=>$value->receiver_id,
                    'alasan_penolakan'=>$value->alasan_penolakan,
                    'qr_code_receiver'=>$value->qr_code_receiver,
                    'request_id'=>$value->request_id,
                    'batch'=>$value->batch,
                ];
                $index = $index + 1;
                // echo $index.'<br/>';
            }

            // if($value->requestStatus == 2 || $value->requestStatus == 4){
            //     $kepada->status = 2;
            // }
            // if($waiting == false && $value->receiver_id == CRUDBooster::myID()){
            //     $kepada[$key]->waiting = false;
            // }elseif ($waiting == false && ($value->requestStatus == 2 || $value->requestStatus == 0)){
            //     $waiting = true;
            // }elseif ($waiting == false && $value->requestStatus == 1){
            //     $waiting = false;
            // }elseif ($waiting == true && $value->receiver_id == CRUDBooster::myID()){
            //     $kepada[$key]->waiting = true;
            // }
        }

        foreach($detailRequest as $index=>$eachBatch){
            $detailRequest[$index]['status']= 1;
            $waiting[$index]= false;
            foreach($eachBatch as $key=>$eachRequest){
                if($eachRequest['requestStatus'] == 2 || $eachRequest['requestStatus'] == 4){
                    $detailRequest[$index]['status'] = 2;
                }
                if($waiting[$index] == false && $eachRequest['receiver_id'] == CRUDBooster::myID()){
                    // echo $index.' '.$key.'1 <br/>';
                    $detailRequest[$index][$key]['waiting'] = false;
                }elseif ($waiting[$index] == false && ($eachRequest['requestStatus'] == 2 || $eachRequest['requestStatus'] == 0)){
                    $waiting[$index] = true;
                    // echo 'masuk <br/>';
                }elseif ($waiting[$index] == false && $eachRequest['requestStatus'] == 1){
                    $waiting[$index] = false;
                }elseif ($waiting[$index] == true && $eachRequest['receiver_id'] == CRUDBooster::myID()){
                    // echo $index.' '.$key.'2 <br/>';
                    $detailRequest[$index][$key]['waiting'] = true;
                }
            }
        }
        // dd(count($detailRequest),$detailRequest, $kepada);
        // dd($getData->tujuan_id, CRUDBooster::myId());

    return view('pages.detail', compact('getData', 'getUser', 'getTipe', 'getJenis', 'getCategory', 'detailRequest', 'getTo'));
    }

    public function accept($id)
    {
        $update = DB::table('tujuan')
        ->where('id', $id)
        ->update([
        'status' => 1,
        ]);
        // generate paraf yang di request
        $getData = DB::table('tujuan')->where('tujuan.id', $id)->join('request_paraf', 'request_paraf.id', 'tujuan.request_id')->first();
        $qrCode = sha1( $getData->sender_id . $getData->documentId . $getData->kategoriId . $getData->jenisId . $getData->tipeId
        . CRUDBooster::myID()) . time();
        $updateQr = DB::table('tujuan')->where('id',$id)->update([
            'qr_code_receiver'=>$qrCode
        ]);
        return redirect()->back();
    }
    public function reject(Request $request, $id, $requestId)
    {
        $getData = DB::table('tujuan')->where('request_id', $requestId)->get();
        
        foreach($getData as $data){
            // mau di reject semua atau satu aja
            if($data->id != $id){
                $update = DB::table('tujuan')
                ->where('id', $id)
                ->update([
                'status' => 2,
                ]);
            }else{
                $update = DB::table('tujuan')
                    ->where('id', $id)
                    ->update([
                    'status' => 2,
                    'alasan_penolakan' => $request->alasan,
                ]);
            }
        }
        
        
    return redirect()->route('detail', ['id' => $requestId]);
    }
    public function rejectPage($id, $requestId)
    {
    return view('pages.rejectPage', compact('id', 'requestId'));
    }

    public function generateQrCode($id)
    {
        $QR = '';
        $qrCode = DB::table('request_paraf')
        ->join('tujuan', 'tujuan.request_id', 'request_paraf.id')
        ->join('category', 'category.id', 'request_paraf.kategori_id')
        ->join('jenis', 'jenis.id', 'request_paraf.jenis_id')
        ->join('tipe', 'tipe.id', 'request_paraf.tipe_id')
        ->where('tujuan.qr_code_receiver', $id)
        ->first();
        return view('pages.generate', compact('QR', 'qrCode'));
    }

    public function delete($id){
        $getRequestId = DB::table('tujuan')->where('tujuan.id', $id)->first();
        $getRequest = DB::table('request_paraf')->where('request_paraf.id', $getRequestId->request_id)->join('tujuan', 'request_paraf.id', 'tujuan.request_id')->get();
        // dd($getRequest);
        foreach($getRequest as $value){
            DB::table('tujuan')->where('id', $value->id)->delete();
        }
        DB::table('request_paraf')->where('id', $getRequestId->request_id)->delete();
        CRUDBooster::redirect('admin/tujuan', 'data telah dihapus', 'success');
    }

    public function reminder($id, $request_id){
        $config['content'] = "Anda memiliki surat untuk ditandatangani";
        $config['to'] = '/admin/detail/'.$request_id;
        $config['id_cms_users'] = [$id];
        \CRUDBooster::sendNotification($config);
        CRUDBooster::redirect('admin/tujuan', 'reminder telah dikirim', 'success');
    }

    // public function generateSelfQrCode($id){
    //     $QR = '';
    //     $qrCode = DB::table('request_paraf')
    //     ->join('tujuan', 'tujuan.request_id', 'request_paraf.id')
    //     ->join('category', 'category.id', 'request_paraf.kategori_id')
    //     ->join('jenis', 'jenis.id', 'request_paraf.jenis_id')
    //     ->join('tipe', 'tipe.id', 'request_paraf.tipe_id')
    //     ->where('request_paraf.qr_code', $id)
    //     ->first();
    //     return view('pages.generate', compact('QR', 'qrCode'));
    // }
}
