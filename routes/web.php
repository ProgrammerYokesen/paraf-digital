<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return Redirect::to('https://paraf.yokesen.com/admin/login');
});


Route::get('/generate/paraf/{id}', 'ParafController@paraf')->name('qrcode');
Route::get('/c/{id}', 'ParafController@show')->name('showParaf');
Route::get('/admin/add-data/{id?}', 'PageController@add')->name('add');
Route::get('/admin/delete-data/{id}', 'ProcessController@delete')->name('delete');
Route::get('/admin/reminder/{id}/{request_id}', 'ProcessController@reminder')->name('reminder');
Route::post('/admin/add-data-request/{id?}', 'ProcessController@addData')->name('addData');
Route::get('/admin/detail/{id}', 'ProcessController@detail')->name('detail');
Route::get('/admin/accept/{id}', 'ProcessController@accept')->name('accept');
Route::get('/admin/reject-page/{id}/{requestId}', 'ProcessController@rejectPage')->name('rejectPage');
Route::post('/admin/reject/{id}/{requestId}', 'ProcessController@reject')->name('reject');
Route::get('/admin/preview/{id}', 'ProcessController@preview')->name('preview');
Route::get('/admin/show-pdf/{id}', 'ProcessController@showPdf')->name('show-pdf');
Route::get('/admin/generate/paraf/{id}', 'ProcessController@generateQrCode')->name('generateQrCode');
Route::get('generate-self/paraf/{id}', 'ProcessController@generateSelfQrCode')->name('generateSelfQrCode');

Route::get('test', function () {
  phpinfo();
});
