@extends("crudbooster::admin_template")

@section('content')
    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Dokumen Id</b> <a class="pull-right">{{ $getData->document_id }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Tipe</b> <a class="pull-right">{{ $getTipe->tipe_name }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Jenis</b> <a class="pull-right">{{ $getJenis->jenis_name }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Kategori</b> <a class="pull-right">{{ $getCategory->category_name }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Deskripsi</b> <a class="pull-right">{{ $getData->deskripsi }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Deadline</b> <a class="pull-right">{{ $getData->due_date }}</a>
                        </li>
                        <li class="list-group-item">
                            {{-- <img src="data:image/png;base64, {!! base64_encode(
    QrCode::format('png')->merge('https://paraf.yokesen.com/images/icon-yokesen.png', 0.4, true)->size(200)->errorCorrection('H')->generate('https://paraf.yokesen.com/generate-self/paraf/' . $tujuan->qr_code_receiver),
) !!} "> --}}
                        </li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <!-- /.col -->


        <div class="col-md-9">
            @foreach ($detailRequest as $batch)
                {{-- <div class="col-md-9"> --}}
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <table class="table">
                            <tbody>
                                @foreach (array_slice($batch, 0, count($batch) - 1) as $person)
                                    <tr>
                                        <td style="width: 33%">{{ $person['userName'] }}</td>
                                        @if ($person['requestStatus'] == 0)
                                            <td style="width: 33%">Status: waiting</td>
                                        @elseif($person['requestStatus'] == 1)
                                            <td style="width: 33%">Status: Accepted</td>
                                        @else
                                            <td style="width: 33%">Status: Rejected</td>
                                        @endif
                                        @if ($person['receiver_id'] == CRUDBooster::myId())
                                            @if ($batch['status'] == 2)
                                                <td style="width: 33%"></td>
                                            @elseif ($person['waiting'] == false && $person['requestStatus'] == 0)
                                                <td style="width: 33%"><a
                                                        href="{{ route('accept', ['id' => $person['tujuanId']]) }}"
                                                        class="btn btn-success" id="accept">Accept</a>
                                                    <a href="{{ route('rejectPage', ['id' => $person['tujuanId'], 'requestId' => $person['request_id']]) }}"
                                                        class="btn btn-danger" id="reject">Reject</a>
                                                </td>
                                            @elseif($person['waiting'] == true ||
                                                (empty($person['alasan_penolakan']) &&
                                                ($person['requestStatus'] == 2 || $person['requestStatus'] == 4)))
                                                <td style="width: 33%"></td>
                                            @elseif (!empty($person['alasan_penolakan']) &&
                                                ($person['requestStatus'] ==
                                                2
                                                ||
                                                $person['requestStatus'] == 4))
                                                <td style="width: 33%">{{ $person['alasan_penolakan'] }}</td>
                                            @else
                                                <td style="width: 33%"></td>

                                            @endif
                                            {{-- @if (property_exists($kepada, 'status') && empty($person->alasan_penolakan) == false && ($person->requestStatus == 4 || $person->requestStatus == 2))
                                    <td>{{ $person->alasan_penolakan }}</td>
                                @elseif(property_exists($kepada,'status') && empty($person->alasan_penolakan)
                                    == true && ($person->requestStatus == 4 || $person->requestStatus == 2))
                                    <td></td>
                                @elseif ($person->requestStatus != 0)
                                    <td></td>
                                @else
                                    <td><a href="{{ route('accept', ['id' => $person->tujuanId]) }}"
                                            class="btn btn-success" id="accept">Accept</a>
                                        <a href="{{ route('rejectPage', ['id' => $person->tujuanId]) }}"
                                            class="btn btn-danger" id="reject">Reject</a>
                                    </td>
                                @endif --}}
                                        @else
                                            @if (($person['requestStatus'] == 2 || $person['requestStatus'] == 4) && empty($person['alasan_penolakan']) == false)
                                                <td style="width: 33%">{{ $person['alasan_penolakan'] }}</td>
                                            @elseif(($person['requestStatus'] == 2 || $person['requestStatus'] == 4)
                                                &&
                                                empty($person['alasan_penolakan'])
                                                == true)
                                                <td style="width: 33%"></td>
                                            @else
                                                <td style="width: 33%"></td>
                                            @endif
                                        @endif

                                        {{-- @if ($person->requestStatus == 1)
                                <td><a href="{{ route('generateQrCode', ['id' => $getData->qr_code]) }}"
                                        class="btn btn-primary">Generate Paraf</a></td>
                            @else
                                <td><a href="" class="btn btn-primary" disabled>Generate Paraf</a></td>
                            @endif --}}
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
                {{-- </div> --}}
                <!-- /.col -->
            @endforeach
        </div>


        {{-- <ul class="list-group list-group-unbordered">
                        @foreach ($members as $member)
                            <li class="list-group-item">
                                <b style="margin-right:30px;">{{ $member->name }}</b>
                                @if ($member->status == 0)
                                    <span>Status: waiting</span>
                                @elseif($member->status == 1)
                                    <span>Status: Accepted</span>
                                @else
                                    <span>Status: Rejected</span>
                                @endif
                                <div><button class="btn btn-success">Accept</button> <button
                                        class="btn btn-danger">Reject</button></div>
                            </li>
                        @endforeach
                    </ul> --}}

    </div>
    <!-- /.row -->
@endsection
@section('js')
    <script type="text/javascript"></script>
@endsection
