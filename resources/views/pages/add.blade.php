@extends("crudbooster::admin_template")

@section('csslist')
    <link href="{{ url('/') }}/vendor/crudbooster/assets/select2/dist/css/select2.min.css" rel="stylesheet"
        type="text/css" />
@endsection
@section('content')
    @if ($data[0])
        <form style="background-color: white; padding: 20px; margin:0 10px 0 10px;" class="col-sm-6" method="POST"
            action="{{ route('addData', ['id' => $data[0]['id']]) }}" enctype="multipart/form-data">

        @else
            <form style="background-color: white; padding: 20px; margin:0 10px 0 10px;" class="col-sm-6" method="POST"
                action="{{ route('addData') }}" enctype="multipart/form-data">
    @endif

    @csrf
    <div class="form-group">
        <label for="exampleInputEmail1">Document Id <span style="color: red;">*<span></label>
        <input type="text" class="form-control" id="documentId" name="documentId" aria-describedby="emailHelp"
            value="{{ $data[0]['document_id'] ? $data[0]['document_id'] : '' }}" required>
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Judul <span style="color: red;">*<span></label>
        <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp"
            value="{{ $data[0]['title'] ? $data[0]['title'] : '' }}" required>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Deskripsi <span style="color: red;">*<span></label>
        <textarea class="form-control" id="description" name=description rows="3"
            required>{{ $data[0]['deskripsi'] ? $data[0]['deskripsi'] : '' }}</textarea>
    </div>
    <div class="form-group upload-section">
        <div class='upload-document-wrapper hidden'>
            <label for="exampleFormControlFile1">Upload Document</label>
            <input type="file" class="form-control-file" id="uploadSurat" name="image">
        </div>
        @if ($data[0]['surat'])
            <div class="preview-document-wrapper">
                <p for="exampleInputPassword1"><b>Document<b /></p>
                <a href="{{ asset($data[0]['surat']) }}" target="_blank" id='preview-document'>Preview
                    Document</a>
                <div class="upload-icon-wrapper">
                    <label for="upload-file"><i class="fa fa-pencil pencil-color"></i></label>
                    <input name="image" type="file" id="upload-file"
                        onchange="document.getElementById('preview-document').href = window.URL.createObjectURL(this.files[0])"
                        style="display:none; visibility:hidden;" />
                </div>

                <i class="fa fa-trash trash-color" id='delete-icon'></i>
            </div>
        @else
            <label for="exampleFormControlFile1">Upload Document</label>
            <input type="file" class="form-control-file" id="uploadSurat" name="image">
        @endif

    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Link Document</label>
        <input type="text" class="form-control" id="link" name="link" aria-describedby="emailHelp"
            value="{{ $data[0]['link'] ? $data[0]['link'] : '' }}">
    </div>
    <div class="form-group">
        <label for="exampleFormControlSelect1">Kategori Id <span style="color: red;">*<span></label>
        <select class="form-control" id="KategoriId" name="kategoriId" required>
            @foreach ($getCategory as $item)
                @if ($data[0]['kategori_id'] && $data[0]['kategori_id'] == $item->id)
                    <option value="{{ $item->id }}" selected>{{ $item->category_name }}</option>
                @else
                    <option value="{{ $item->id }}">{{ $item->category_name }}</option>
                @endif
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="exampleFormControlSelect1">Jenis Id <span style="color: red;">*<span></label>
        <select class="form-control" id="jenisId" name="jenisId" required>
            @foreach ($getJenis as $item)
                @if ($data[0]['jenis_id'] && $data[0]['jenis_id'] == $item->id)
                    <option value="{{ $item->id }}" selected>{{ $item->jenis_name }}</option>
                @else
                    <option value="{{ $item->id }}">{{ $item->jenis_name }}</option>
                @endif
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="exampleFormControlSelect1">Tipe Id <span style="color: red;">*<span></label>
        <select class="form-control" id="tipeId" name="tipeId" required>
            @foreach ($getTipe as $item)
                @if ($data[0]['tipe_id'] && $data[0]['tipe_id'] == $item->id)
                    <option value="{{ $item->id }}" selected>{{ $item->tipe_name }}</option>
                @else
                    <option value="{{ $item->id }}">{{ $item->tipe_name }}</option>
                @endif
            @endforeach
        </select>
    </div>
    <div class="form-group">

        <label for="exampleInputEmail1">Deadline <span style="color: red;">*<span></label>
        <input type="date" class="form-control" id="deadline" name="deadline" aria-describedby="emailHelp"
            value="{{ $data[0]['due_date'] ? \Carbon\Carbon::parse($data[0]['due_date'])->format('Y-m-d') : '' }}"
            required>
    </div>
    <div class=" form-group">
        <label for="exampleFormControlSelect1">Kepada <span style="color: red;">*<span></label>
        <select class="form-control" multiple="multiple" id="receiverId" name="receiverId[]" style="width: 100%" required>
            {{-- <option value="" selected disabled>Pilih Anggota</option> --}}
            @foreach ($getAllUsers as $user)
                <option value="{{ $user->id }}">{{ $user->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="list-member mb-3" id='members'>

    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

@section('js')

    <script src="{{ url('/') }}/vendor/crudbooster/assets/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript">
        var receiverString = "<?php echo $data[0]['receiverId']; ?>";
        var receiverArray = receiverString.split(',');

        console.log(receiverArray);
        $('#receiverId').select2().val(receiverArray).trigger('change');
        $(document).ready(function() {

            $("#delete-icon").on('click', function() {
                $('.upload-document-wrapper').removeClass('hidden')
                $('.preview-document-wrapper').addClass('hidden')
            });
        })
    </script>
    <script>
        // $(document).on('click', '.remove', function() {
        //     var tes = $(this).parent().remove();
        //     console.log(tes);
        // });
    </script>
@endsection


<!--<select class="form-control" id="name" name="name" required>-->
<!--    @foreach ($getAllUsers as $user) -->
                        <!--        <option value="{{ $user->id }}">{{ $user->name }}</option>-->
                        <!-- @endforeach-->
<!--</select>-->
