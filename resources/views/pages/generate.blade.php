@extends("crudbooster::admin_template")

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="box box-info">
                <div class="box-header text-center">
                    <h4>Paraf Digital - copy code ini</h4>
                </div>
                <div class="box-body text-center">

                    <img src="data:image/png;base64, {!! base64_encode(
    QrCode::format('png')->merge('https://paraf.yokesen.com/images/icon-yokesen.png', 0.4, true)->size(200)->errorCorrection('H')->generate('https://paraf.yokesen.com/c/' . $qrCode->qr_code),
) !!} ">
                    <img src="{{ $QR }}">
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <b>Deskripsi</b>
                        </div>
                        <div class="col-md-6">
                            {!! nl2br(e($qrCode->deskripsi)) !!}
                        </div>
                    </div>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Nomor Dokumen</b> <a class="pull-right">{{ $qrCode->document_id }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Judul</b> <a class="pull-right">{{ $qrCode->title }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Kategori</b> <a class="pull-right">{{ $qrCode->category_name }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Jenis</b> <a class="pull-right">{{ $qrCode->jenis_name }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Tipe</b> <a class="pull-right">{{ $qrCode->tipe_name }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    @for ($i = 0; $i < $count; $i++)

    @endfor
@endsection
