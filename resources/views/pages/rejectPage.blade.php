@extends("crudbooster::admin_template")

@section('content')
    <form action="{{ route('reject', ['id' => $id, 'requestId' => $requestId]) }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Alasan Penolakan</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="alasan" required></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
