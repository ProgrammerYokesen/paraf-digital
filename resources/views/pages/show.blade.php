<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Konfirmasi Paraf </title>

    <link rel="shortcut icon" href="{{ url('/') }}/uploads/2021-07/83ea0e55d46f83ef65996418d4c0f091.png">

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ url('/') }}/vendor/crudbooster/assets/adminlte/bootstrap/css/bootstrap.min.css" rel="stylesheet"
        type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"
        type="text/css" />
    <!-- Theme style -->
    <link href="{{ url('/') }}/vendor/crudbooster/assets/adminlte/dist/css/AdminLTE.min.css" rel="stylesheet"
        type="text/css" />

    <!-- support rtl-->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <link rel='stylesheet' href='{{ url('/') }}/vendor/crudbooster/assets/css/main.css' />
    <style type="text/css">
        .login-page,
        .register-page {
            background: #dddddd url('{{ url('/') }}/uploads/2021-07/64e7f3fe4b96c5888513028ba83bd86d.png');
            color: #ffffff !important;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }

        .login-box,
        .register-box {
            margin: 2% auto;

        }

        .login-box-body {
            box-shadow: 0px 0px 50px rgba(0, 0, 0, 0.8);
            background: rgba(255, 255, 255, 0.9);
            color: #666666 !important;
            opacity: 1 !important;
        }

        html,
        body {
            overflow: hidden;
        }

    </style>
</head>

<body class="login-page">

    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('/') }}">
                <img title='Paraf Yokesen'
                    src='{{ url('/') }}/uploads/2021-07/1b001ff1ebaf3adf5a32ca54b718830d.png'
                    style='max-width: 100%;max-height:170px' />
            </a>
        </div><!-- /.login-logo -->
        <div class="login-box-body" style="padding:20px;">
            <p> <b>Berkas :</b> </p>
            <p style="background-color:white!important;padding: 10px;">{!! nl2br(e($paraf->deskripsi)) !!}</p>
            <ul class="list-group list-group-unbordered">
                <li class="list-group-item" style="padding:15px;">
                    <b>Nomor Dokumen</b> <a class="pull-right">{{ $paraf->document_id }}</a>
                </li>
                <li class="list-group-item" style="padding:15px;">
                    <b>Stempel</b> <a
                        class="pull-right">{{ date('d-m-Y H:i:s', strtotime($paraf->time_signed . '+ 7 hours')) }} WIB</a>
                </li>
                <li class="list-group-item" style="padding:15px;">
                    <b>Kategori</b> <a class="pull-right">{{ $paraf->category_name }}</a>
                </li>
                <li class="list-group-item" style="padding:15px;">
                    <b>Jenis</b> <a class="pull-right">{{ $paraf->jenis_name }}</a>
                </li>
                <li class="list-group-item" style="padding:15px;">
                    <b>Diparaf oleh</b> <a class="pull-right">{{ $paraf->name }}</a>
                </li>
                <li class="list-group-item" style="padding:15px;">
                    <b>Kredensial</b> <a class="pull-right">{{ $paraf->email }}</a>
                </li>
                <li class="list-group-item" style="padding:15px;">
                    <b>Diverifikasi</b> <a class="pull-right">PT. Yokesen Teknologi Indonesia</a>
                </li>
            </ul>

        </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->


    <!-- jQuery 2.2.3 -->
    <script src="{{ url('/') }}/vendor/crudbooster/assets/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.4.1 JS -->
    <script src="{{ url('/') }}/vendor/crudbooster/assets/adminlte/bootstrap/js/bootstrap.min.js"
        type="text/javascript"></script>
</body>

</html>
